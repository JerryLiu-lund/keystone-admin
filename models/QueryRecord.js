var keystone = require('keystone');
var Types = keystone.Field.Types;

// storage for the files to be uploaded
var myStorage = new keystone.Storage({
  adapter: keystone.Storage.Adapters.FS,
  fs: {
    path: keystone.expandPath('./public/uploads/documents'),
    /*publicPath: '/public/uploads/documents',*/
    publicPath: 'http://res.cloudinary.com/dy8kdozhn/image/upload/',
  },
  schema: {
    size: true,
    mimetype: true,
    path: true,
    originalname: true,
    url: true
  }
});

var QueryRecord = new keystone.List('QueryRecord', {
	autokey: { from: 'recordNo', path: 'slug', unique: true },
	map: { name: 'recordNo' },
	defaultSort: '-recordNo'
} );

QueryRecord.add({
  recordNo: { type: String, required: true, initial: true, default: 'No.' },
  recordProduct: { type: String, required: true, initial: true },
  companyName: { type: String, required: true, initial: true },
  industry: { type: String, required: true, initial: true },
  department: { type: String, required: true, initial: true },
  publishedDate: { type: Date, default: Date.now },
  contactName: { type: Types.Name },
  contactSex: { type: String },
  contactEmail: { type: Types.Email },
  contactPhone: { type: String },
  contactAddress: { type: Types.Location },
  contactProvince: { type: String },
  contactPosition: { type: String },
  contactPhone: { type: String },
  budget: {type: Types.Money},
});


// virtuals added to the schema
QueryRecord.schema.virtual('url').get(function(){
  return '/QueryRecord/' + this.slug;
});

/**
 * Registration
 */
QueryRecord.defaultColumns = 'recordNo|20%, contactName|16%, budget, publishedDate';
QueryRecord.register();
