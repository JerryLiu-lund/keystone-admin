var keystone = require('keystone');
var Types = keystone.Field.Types;

// storage for the files to be uploaded
var myStorage = new keystone.Storage({
  adapter: keystone.Storage.Adapters.FS,
  fs: {
    path: keystone.expandPath('./public/uploads/documents'),
    /*publicPath: '/public/uploads/documents',*/
    publicPath: 'http://res.cloudinary.com/dy8kdozhn/image/upload/',
  },
  schema: {
    size: true,
    mimetype: true,
    path: true,
    originalname: true,
    url: true
  }
});

var Evaluation = new keystone.List('Evaluation', {
	autokey: { from: 'evaluationName', path: 'slug', unique: true },
	map: { name: 'evaluationName' },
	defaultSort: '-evaluationName'
} );

Evaluation.add({
  evaluationName: { type: String, required: true, initial: true, default: 'New Evaluation' },
  evaluationCategory: { type: String, required: true, initial: true, default: 'Category' },
  publishedDate: { type: Date, default: Date.now },
  heroImage: { type: Types.CloudinaryImage, required: true, initial: true },
});


// virtuals added to the schema
Evaluation.schema.virtual('url').get(function(){
  return '/Evaluation/' + this.slug;
});

/**
 * Registration
 */
Evaluation.defaultColumns = 'evaluationName|30%, evaluationCategory|30%, publishedDate';
Evaluation.register();
